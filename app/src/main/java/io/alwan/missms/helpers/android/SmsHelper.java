package io.alwan.missms.helpers.android;

public interface SmsHelper {

    void sendMessage(String textToSend, String addressToSendTo);

    boolean checkIfDefaultSMS();


}
