package io.alwan.missms.helpers.android;

import java.util.ArrayList;
import java.util.HashMap;

import io.alwan.missms.entities.SMS;

public interface NotificationHelper {

    void updateNotifications(boolean silent, HashMap<String, ArrayList<SMS>> unreadMessages);

    void dismissThread(String threadId);

}
