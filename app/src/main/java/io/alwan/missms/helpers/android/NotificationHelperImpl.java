package io.alwan.missms.helpers.android;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.provider.Settings;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.RemoteInput;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.alwan.missms.R;
import io.alwan.missms.entities.Contact;
import io.alwan.missms.entities.SMS;
import io.alwan.missms.injections.ApplicationContext;
import io.alwan.missms.receivers_modules.NotificationReceivers;
import io.alwan.missms.views.MainActivity;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * Created by Ali-Al1 on 08/07/2017.
 */
@Singleton
public class NotificationHelperImpl implements NotificationHelper {

    public static final String KEY_TEXT_REPLY = "key_text_reply";
    public final static String SENT = "SMS_SENT";
    public final static String READ = "READ_THREAD";
    public final static String SEND = "SEND_SMS";
    private final int SUMMARY_ID = 0;
    private Context context;
    private NotificationManager notificationManager;
    private ContactHelper contactHelper;

    @Inject
    NotificationHelperImpl(@ApplicationContext Context context, ContactHelper contactHelper) {

        this.context = context;
        notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);


        this.contactHelper = contactHelper;
    }

    private static Notification buildSummary(Context context, String appName, ArrayList<SMS> latestMessages, boolean silent) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setWhen(Long.parseLong(latestMessages.get(latestMessages.size() - 1).getDate()))
                        .setSmallIcon(R.drawable.ic_logo)
                        .setColor(Color.BLACK)
                        .setShowWhen(true)
                        .setGroup(appName)
                        .setLights(Color.BLUE, 3000, 3000)
                        .setGroupSummary(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && !silent) {
            mBuilder.setPriority(Notification.PRIORITY_HIGH);
        }

        if (!silent)
            mBuilder.setVibrate(new long[]{500, 500})
                    .setSound(Settings.System.DEFAULT_NOTIFICATION_URI);

        // Creates an explicit intent for an Activity in my app
        Intent resultIntent = new Intent(context, MainActivity.class);

        //Set pending intent for notification
        PendingIntent pendingIntent = PendingIntent.getActivity(
                context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);

        return mBuilder.build();
    }

    @Override
    public void updateNotifications(boolean silent, HashMap<String, ArrayList<SMS>> unreadMessages) {

        if (unreadMessages.size() != 0) {

            ArrayList<SMS> latestMessages = new ArrayList<>();

            for (Map.Entry<String, ArrayList<SMS>> entry : unreadMessages.entrySet()) {
                latestMessages.add(entry.getValue().get(0));
            }

            String appName = context.getResources().getString(R.string.app_name);

            for (SMS sms : latestMessages) {
                Contact contact = contactHelper.getContact(sms.getAddress());
                Notification notification = buildNotification(sms, appName, contact,
                        unreadMessages.get(sms.getAddress()), silent);
                notificationManager.notify(Integer.parseInt(sms.getThreadId()), notification);
            }
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Notification summary = buildSummary(context, appName, latestMessages, silent);
                notificationManager.notify(SUMMARY_ID, summary);
            }
        }
    }

    @Override
    public void dismissThread(String threadId) {
        notificationManager.cancel(Integer.parseInt(threadId));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            StatusBarNotification[] activeNotifications = notificationManager.getActiveNotifications();
            if (activeNotifications.length == 1)
                notificationManager.cancel(SUMMARY_ID);
        }
    }

    private Notification buildNotification(SMS sms, String appName,
                                           Contact contact, ArrayList<SMS> unreadMessages,
                                           boolean silent) {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setContentTitle(sms.getAddress())
                        .setContentText(sms.getBody())
                        .setWhen(Long.parseLong(sms.getDate()))
                        .setShowWhen(true)
                        .setNumber(unreadMessages.size() - 1)
                        .setGroup(appName)
                        .setSmallIcon(R.drawable.ic_logo)
                        .setColor(Color.BLACK);

        if (!silent)
            mBuilder.setVibrate(new long[]{500, 500})
                    .setLights(Color.BLUE, 3000, 3000)
                    .setSound(Settings.System.DEFAULT_NOTIFICATION_URI);

        if (contact != null) {

            String contactName = contact.getContactName();
            if (contactName != null)
                mBuilder.setContentTitle(contactName);

            String contactId = contact.getContactId();
            if (contactId != null) {
                mBuilder.addPerson(contactHelper.getPersonUri(contactId));
            }
            Bitmap contactDP = contact.getContactDP();
            if (contactDP != null)
                mBuilder.setLargeIcon(contactDP);

        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            // Key for the string that's delivered in the action's intent.
            String replyLabel = context.getResources().getString(R.string.reply_label);
            RemoteInput remoteInput = new RemoteInput.Builder(KEY_TEXT_REPLY)
                    .setLabel(replyLabel)
                    .build();

            // Creates an explicit intent for an Activity in your app
            Intent resultIntent = new Intent(context, NotificationReceivers.class);
            resultIntent.setAction(SEND);
            resultIntent.putExtra("addressForSend", sms.getAddress());
            resultIntent.putExtra("threadForSend", sms.getThreadId());


            PendingIntent replyPendingIntent = PendingIntent.getBroadcast(context,
                    Integer.parseInt(sms.getThreadId()),
                    resultIntent, PendingIntent.FLAG_CANCEL_CURRENT);

            NotificationCompat.Action action =
                    new NotificationCompat.Action.Builder(R.drawable.ic_send,
                            context.getString(R.string.reply_label), replyPendingIntent)
                            .addRemoteInput(remoteInput)
                            .setAllowGeneratedReplies(true)
                            .build();

            mBuilder.addAction(action);
        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            // Creates an explicit intent for an Activity in your app
            Intent resultIntent = new Intent(context, NotificationReceivers.class);

            // Sets the Activity to start in a new, empty task
            resultIntent.setFlags(FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .putExtra("threadForRead", sms.getThreadId())
                    .setAction(READ);

            PendingIntent replyPendingIntent = PendingIntent.getBroadcast(context,
                    Integer.parseInt(sms.getThreadId()),
                    resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Action action =
                    new NotificationCompat.Action.Builder(R.drawable.ic_check,
                            "Read", replyPendingIntent)
                            .build();

            mBuilder.addAction(action);
        }

        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();

        // Moves events into the expanded layout
        int unreadSize = unreadMessages.size();
        Collections.reverse(unreadMessages);

        if (unreadSize > 5)
            for (int i = unreadSize - 5; i < unreadSize; i++) {
                inboxStyle.addLine(unreadMessages.get(i).getBody());
            }

        else
            for (SMS message : unreadMessages) {
                inboxStyle.addLine(message.getBody());
            }

        // Moves the expanded layout object into the notification object.
        mBuilder.setStyle(inboxStyle);

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, MainActivity.class);
        // Sets the Activity to start in a new, empty task
        resultIntent.setFlags(FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        resultIntent.putExtra("address", sms.getAddress());

        //Set pending intent for notification
        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                Integer.parseInt(sms.getThreadId()), resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);

        return mBuilder.build();
    }
}
