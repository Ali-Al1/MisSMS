package io.alwan.missms.helpers.android;

import io.alwan.missms.entities.Contact;

public interface ContactHelper {

    Contact getContact(String number);

    Contact getProfile();

    String getPersonUri(String contactId);
}
