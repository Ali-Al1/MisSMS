package io.alwan.missms.helpers.db;


import java.util.ArrayList;
import java.util.HashMap;

import io.alwan.missms.entities.SMS;

public interface DbHelper {

    ArrayList<SMS> getSmsList(String uriString, String[] columns, String selection,
                              String[] selectionArgs, String sortOrder);

    HashMap<String, ArrayList<SMS>> getSmsHash(String uriString,
                                               String[] columns, String selection,
                                               String[] selectionArgs,
                                               String sortOrder);

    void writeSMS(String senderNum, String message, String location);

    void markThreadAsRead(String threadId);

    void deleteThread(String threadId);

    void deleteMessageById(String id);

    void updateStatus(String address, String body, int error);

    void unreadMessageById(String id);

    void updateNotifications(boolean silent);

    ArrayList<SMS> getConversation(String address);

    ArrayList<SMS> getConversations();

}
