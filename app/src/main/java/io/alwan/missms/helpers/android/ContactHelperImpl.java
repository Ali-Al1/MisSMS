package io.alwan.missms.helpers.android;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;

import java.io.IOException;
import java.io.InputStream;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.alwan.missms.entities.Contact;
import io.alwan.missms.injections.ApplicationContext;

/**
 * Created by Ali-Al1 on 08/07/2017.
 */
@Singleton
public class ContactHelperImpl implements ContactHelper {
    private Context context;
    private PermissionHelper permissionHelper;

    @Inject
    public ContactHelperImpl(@ApplicationContext Context context, PermissionHelper permissionHelper) {
        this.context = context;
        this.permissionHelper = permissionHelper;
    }

    @Override
    public Contact getContact(String number) {
        if (number == null) {
            return null;
        } else {
            if (permissionHelper.checkPermissionAndRequest(Manifest.permission.READ_CONTACTS)) {

                ContentResolver contentResolver = context.getContentResolver();
                String contactId = null;
                String contactName = null;
                String contactNumber = null;

                Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

                String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID};

                Cursor cursor = contentResolver.query(uri, projection, null, null, null);

//                Log.i("test", "getContact: " + DatabaseUtils.dumpCursorToString(cursor));

                if (cursor != null) {
                    while (cursor.moveToNext()) {
                        contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
                        contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        contactNumber = cursor.getString(cursor.getColumnIndex("number"));
                    }
                    cursor.close();
                }

                Bitmap photo = null;
                if (contactId != null)
                    try {
                        InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(context.getContentResolver(),
                                ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.valueOf(contactId)));
                        if (inputStream != null) {
                            photo = BitmapFactory.decodeStream(inputStream);
                            inputStream.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                Contact contact = new Contact();

                if (contactNumber != null)
                    contact.setContactAddress(contactNumber);
                else contact.setContactAddress(number);
                contact.setContactDP(photo);
                contact.setContactName(contactName);
                contact.setContactId(contactId);


                return contact;
            } else return null;
        }
    }

    @Override
    public Contact getProfile() {
        if (permissionHelper.checkPermissionAndRequest(Manifest.permission.READ_CONTACTS)) {
            ContentResolver contentResolver = context.getContentResolver();
            String contactId = null;
            String contactName = null;
            String contactNumber;

            Uri uri1 = ContactsContract.Profile.CONTENT_URI;

            Cursor cursor =
                    contentResolver.query(uri1, null, null, null, null);

            if (cursor != null) {
                while (cursor.moveToNext()) {
                    contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
                    contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                }
                cursor.close();
            }

            Bitmap photo = null;
            if (contactId != null)
                try {
                    InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(context.getContentResolver(),
                            ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.valueOf(contactId)));
                    if (inputStream != null) {
                        photo = BitmapFactory.decodeStream(inputStream);
                        inputStream.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            Contact contact = new Contact();

            contact.setContactDP(photo);
            contact.setContactName(contactName);

            TelephonyManager tMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            contactNumber = tMgr.getLine1Number();

            contact.setContactAddress(contactNumber);

            return contact;
        } else return null;
    }

    @Override
    public String getPersonUri(String contactId) {
        String personUri = null;
        Cursor c = null;
        try {
            String[] projection = new String[]{ContactsContract.Contacts._ID, ContactsContract.Contacts.LOOKUP_KEY};
            String selections = ContactsContract.Contacts._ID + " = " + contactId;
            final ContentResolver contentResolver = context.getContentResolver();
            c = contentResolver.query(ContactsContract.Contacts.CONTENT_URI,
                    projection, selections, null, null);
            if (c != null && c.getCount() > 0) {
                c.moveToFirst();
                int lookupIdx = c.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY);
                int idIdx = c.getColumnIndex(ContactsContract.Contacts._ID);
                String lookupKey = c.getString(lookupIdx);
                long contactIdd = c.getLong(idIdx);
                Uri lookupUri = ContactsContract.Contacts.getLookupUri(contactIdd, lookupKey);
                personUri = lookupUri.toString();
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return personUri;
    }
}
