package io.alwan.missms.helpers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.alwan.missms.entities.SMS;
import io.alwan.missms.helpers.db.DbHelper;

/**
 * Created by Ali-Al1 on 08/07/2017.
 */
@Singleton
public class DataManagerImpl implements DataManager {
    private DbHelper dbHelper;

    @Inject
    DataManagerImpl(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    @Override
    public ArrayList<SMS> getSmsList(String uriString, String[] columns, String selection,
                                     String[] selectionArgs, String sortOrder) {
        return dbHelper.getSmsList(uriString, columns, selection, selectionArgs, sortOrder);
    }

    @Override
    public HashMap<String, ArrayList<SMS>> getSmsHash(String uriString, String[] columns, String selection, String[] selectionArgs, String sortOrder) {
        return dbHelper.getSmsHash(uriString, columns, selection, selectionArgs, sortOrder);
    }

    @Override
    public void writeSMS(String senderNum, String message, String location) {
        dbHelper.writeSMS(senderNum, message, location);
    }

    @Override
    public void markThreadAsRead(String threadId) {
        dbHelper.markThreadAsRead(threadId);
    }

    @Override
    public void deleteThread(String threadId) {
        dbHelper.deleteThread(threadId);
    }

    @Override
    public void deleteMessageById(String id) {
        dbHelper.deleteThread(id);
    }

    @Override
    public void updateStatus(String address, String body, int error) {
        dbHelper.updateStatus(address, body, error);
    }

    @Override
    public void unreadMessageById(String id) {
        dbHelper.unreadMessageById(id);
    }

    @Override
    public void updateNotifications(boolean silent) {
        dbHelper.updateNotifications(silent);
    }

    @Override
    public ArrayList<SMS> getConversation(String address) {
        return dbHelper.getConversation(address);
    }

    @Override
    public ArrayList<SMS> getConversations() {
        return dbHelper.getConversations();
    }
}
