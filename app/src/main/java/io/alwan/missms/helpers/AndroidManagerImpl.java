package io.alwan.missms.helpers;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.alwan.missms.entities.Contact;
import io.alwan.missms.entities.SMS;
import io.alwan.missms.helpers.android.ContactHelper;
import io.alwan.missms.helpers.android.NotificationHelper;
import io.alwan.missms.helpers.android.PermissionHelper;
import io.alwan.missms.helpers.android.SmsHelper;
import io.alwan.missms.injections.ApplicationContext;

/**
 * Created by Ali-Al1 on 05/07/2017.
 */

@Singleton
public class AndroidManagerImpl implements AndroidManager {

    private Context context;
    private NotificationHelper notificationHelper;
    private PermissionHelper permissionHelper;
    private ContactHelper contactHelper;
    private SmsHelper smsHelper;

    @Inject
    public AndroidManagerImpl(@ApplicationContext Context context,
                              NotificationHelper notificationHelper,
                              PermissionHelper permissionHelper, ContactHelper contactHelper,
                              SmsHelper smsHelper) {

        this.context = context;
        this.notificationHelper = notificationHelper;
        this.permissionHelper = permissionHelper;
        this.contactHelper = contactHelper;
        this.smsHelper = smsHelper;
    }

    @Override
    public void copyText(String text) {
        ClipboardManager clipboard =
                (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);

        ClipData clip = ClipData.newPlainText("text", text);

        clipboard.setPrimaryClip(clip);
    }

    @Override
    public void updateNotifications(boolean silent, HashMap<String, ArrayList<SMS>> unreadMessages) {
        notificationHelper.updateNotifications(silent, unreadMessages);
    }

    @Override
    public void dismissThread(String threadId) {
        notificationHelper.dismissThread(threadId);
    }

    @Override
    public boolean checkPermissionAndRequest(String permission) {
        return permissionHelper.checkPermissionAndRequest(permission);
    }

    @Override
    public Contact getContact(String number) {
        return contactHelper.getContact(number);
    }

    @Override
    public Contact getProfile() {
        return contactHelper.getProfile();
    }

    @Override
    public String getPersonUri(String contactId) {
        return contactHelper.getPersonUri(contactId);
    }

    @Override
    public void sendMessage(String textToSend, String addressToSendTo) {
        smsHelper.sendMessage(textToSend, addressToSendTo);
    }

    @Override
    public boolean checkIfDefaultSMS() {
        return smsHelper.checkIfDefaultSMS();
    }
}
