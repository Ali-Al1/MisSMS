package io.alwan.missms.helpers.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.alwan.missms.entities.SMS;
import io.alwan.missms.helpers.android.NotificationHelper;
import io.alwan.missms.injections.ApplicationContext;

/**
 * Created by Ali-Al1 on 08/07/2017.
 */
@Singleton
public class DbHelperImpl implements DbHelper {

    private Context context;
    private NotificationHelper notificationHelper;

    @Inject
    DbHelperImpl(@ApplicationContext Context context, NotificationHelper notificationHelper) {
        this.context = context;
        this.notificationHelper = notificationHelper;
    }

    private static HashMap<String, ArrayList<SMS>> cursorToHash(Cursor c) {
        return smsListToHash(cursorToList(c));
    }

    private static HashMap<String, ArrayList<SMS>> smsListToHash(List<SMS> listSms) {
        HashMap<String, ArrayList<SMS>> smsHash = new HashMap<>();

        for (int i = 0; i < listSms.size(); i++) {
            String currentId = listSms.get(i).getAddress();
            if (!smsHash.containsKey(currentId)) {
                ArrayList<SMS> list = new ArrayList<>();
                list.add(listSms.get(i));
                smsHash.put(currentId, list);
            } else {
                smsHash.get(currentId).add(listSms.get(i));
            }
        }
        return smsHash;
    }

    private static ArrayList<SMS> cursorToList(Cursor c) {
        SMS sms;
        ArrayList<SMS> conversationList = new ArrayList<>();
        int cursorCount = c.getCount();

//        Log.d("", "getConversations: " + DatabaseUtils.dumpCursorToString(c));

        if (c.moveToFirst()) {
            for (int i = 0; i < cursorCount; i++) {
                sms = new SMS();
                sms.setId(c.getString(c.getColumnIndexOrThrow("_id")));
                sms.setThreadId(c.getString(c.getColumnIndexOrThrow("thread_id")));
                sms.setAddress(c.getString(c.getColumnIndexOrThrow("address")));
                sms.setBody(c.getString(c.getColumnIndexOrThrow("body")));
                sms.setRead(c.getString(c.getColumnIndex("read")));
                sms.setDate(c.getString(c.getColumnIndexOrThrow("date")));
                sms.setErrorCode(c.getString(c.getColumnIndexOrThrow("error_code")));
                if (c.getString(c.getColumnIndexOrThrow("type")).contains("1")) {
                    sms.setType("received");
                } else {
                    sms.setType("sent");
                }
                conversationList.add(sms);
                c.moveToNext();
            }
        }
        c.close();

        return conversationList;
    }

    @Override
    public ArrayList<SMS> getSmsList(String uriString, String[] columns, String selection, String[] selectionArgs, String sortOrder) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.parse(uriString);
        return cursorToList(cr.query(uri, columns, selection, selectionArgs, sortOrder));
    }

    @Override
    public HashMap<String, ArrayList<SMS>> getSmsHash(String uriString, String[] columns, String selection, String[] selectionArgs, String sortOrder) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.parse(uriString);
        return cursorToHash(cr.query(uri, columns, selection, selectionArgs, sortOrder));
    }

    @Override
    public void writeSMS(String senderNum, String message, String location) {
        ContentValues values = new ContentValues();
        values.put("address", senderNum);
        values.put("body", message);
        context.getContentResolver().insert(Uri.parse("content://sms/" + location), values);
    }

    @Override
    public void markThreadAsRead(String threadId) {
        Uri uri = Uri.parse("content://sms/inbox");
        ContentResolver contentResolver = context.getContentResolver();

        Cursor cursor = contentResolver.query(uri, null, null, null, null);

        while (cursor.moveToNext()) {
            if ((cursor.getString(cursor.getColumnIndex("thread_id")).equals(threadId))
                    && (cursor.getInt(cursor.getColumnIndex("read")) == 0)) {

                String SmsMessageId = cursor.getString(cursor.getColumnIndex("_id"));
                ContentValues values = new ContentValues();
                values.put("read", true);
                contentResolver.update(Uri.parse("content://sms/inbox"),
                        values, "_id=" + SmsMessageId, null);

            }
        }
        cursor.close();

        notificationHelper.dismissThread(threadId);
    }

    @Override
    public void deleteThread(String threadId) {
        context.getContentResolver().delete(Uri.parse("content://sms/conversations/" + threadId), null, null);
    }

    @Override
    public void deleteMessageById(String id) {
        context.getContentResolver().delete(Uri.parse("content://sms/" + id), null, null);
    }

    @Override
    public void updateStatus(String address, String body, int error) {
        Uri uri = Uri.parse("content://sms/sent");

        ContentResolver contentResolver = context.getContentResolver();

        Cursor cursor = contentResolver.query(uri, null, null, null, null);

        while (cursor.moveToNext()) {
            if ((cursor.getString(cursor.getColumnIndex("address")).equals(address))
                    && (cursor.getString(cursor.getColumnIndex("body")).equals(body))) {

                String SmsMessageId = cursor.getString(cursor.getColumnIndex("_id"));
                ContentValues values = new ContentValues();
                values.put("error_code", error);

                contentResolver.update(uri, values, "_id=" + SmsMessageId, null);

            }
        }
        cursor.close();
    }

    @Override
    public void unreadMessageById(String id) {
        ContentValues values = new ContentValues();

        values.put("read", false);

        context.getContentResolver().update(Uri.parse("content://sms/inbox"),
                values, "_id=" + id, null);
    }

    @Override
    public void updateNotifications(boolean silent) {
        String uriString = "content://sms/";
        String mSelectionClause = "read = 0";

        HashMap<String, ArrayList<SMS>> unreadMessages = getSmsHash(uriString,
                null, mSelectionClause, null, null);

        notificationHelper.updateNotifications(silent, unreadMessages);
    }

    @Override
    public ArrayList<SMS> getConversation(String address) {
        String uriString = "content://sms/";
        String mSelectionClause = "address = ?";
        String[] mSelectionArgs = {address};
        return getSmsList(uriString, null, mSelectionClause, mSelectionArgs, null);
    }

    @Override
    public ArrayList<SMS> getConversations() {
        String uriString = "content://sms//";
        String selection = "_id IS NOT NULL) GROUP BY (thread_id) HAVING date=MAX(date";
        return getSmsList(uriString, null, selection, null,
                null);
    }
}