package io.alwan.missms.helpers;

import io.alwan.missms.helpers.db.DbHelper;
import io.alwan.missms.helpers.prefs.PreferencesHelper;


public interface DataManager extends DbHelper, PreferencesHelper {

}