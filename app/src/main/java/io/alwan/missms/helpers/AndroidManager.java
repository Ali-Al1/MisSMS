package io.alwan.missms.helpers;

import io.alwan.missms.helpers.android.ContactHelper;
import io.alwan.missms.helpers.android.NotificationHelper;
import io.alwan.missms.helpers.android.PermissionHelper;
import io.alwan.missms.helpers.android.SmsHelper;

/**
 * Created by MSI on 05/07/2017.
 */

public interface AndroidManager extends NotificationHelper, PermissionHelper, SmsHelper, ContactHelper {

    void copyText(String text);

}
