package io.alwan.missms.helpers.android;

public interface PermissionHelper {

    boolean checkPermissionAndRequest(String permission);

}
