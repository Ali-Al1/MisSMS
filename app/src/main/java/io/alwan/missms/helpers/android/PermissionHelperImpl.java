package io.alwan.missms.helpers.android;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.alwan.missms.injections.ApplicationContext;

/**
 * Created by Ali-Al1 on 08/07/2017.
 */

@Singleton
public class PermissionHelperImpl implements PermissionHelper {
    private Context context;

    @Inject
    public PermissionHelperImpl(@ApplicationContext Context context) {
        this.context = context;
    }

    @Override
    public boolean checkPermissionAndRequest(String permission) {
        if (checkPermission(permission)) {
            return true;
        } else {
            Activity activity = (Activity) context;
            ActivityCompat.requestPermissions(activity, new String[]{permission}, 2);
            return false;
        }

    }

    private boolean checkPermission(String permission) {
        return ContextCompat.checkSelfPermission(context, permission)
                == PackageManager.PERMISSION_GRANTED;
    }
}
