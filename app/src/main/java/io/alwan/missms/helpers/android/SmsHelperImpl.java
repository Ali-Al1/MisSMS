package io.alwan.missms.helpers.android;

import android.Manifest;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Telephony;
import android.telephony.SmsManager;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.alwan.missms.injections.ApplicationContext;

import static io.alwan.missms.helpers.android.NotificationHelperImpl.SENT;

/**
 * Created by Ali-Al1 on 08/07/2017.
 */
@Singleton
public class SmsHelperImpl implements SmsHelper {
    private final SmsManager smsManager;
    private Context context;
    private PermissionHelper permissionHelper;

    @Inject
    public SmsHelperImpl(@ApplicationContext Context context, PermissionHelper permissionHelper) {
        this.context = context;
        this.permissionHelper = permissionHelper;
        smsManager = SmsManager.getDefault();
    }

    @Override
    public void sendMessage(String textToSend, String addressToSendTo) {
        if (permissionHelper.checkPermissionAndRequest(Manifest.permission.SEND_SMS))
            sendSMS(textToSend, addressToSendTo);

    }

    private void sendSMS(String textToSend, String addressToSendTo) {
        Intent sentIntent = new Intent(SENT);

        sentIntent.putExtra("body", textToSend);
        sentIntent.putExtra("address", addressToSendTo);

        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, sentIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        smsManager.sendTextMessage(addressToSendTo, null, textToSend, sentPI, null);

        ContentValues values = new ContentValues();
        values.put("address", addressToSendTo);
        values.put("body", textToSend);
        context.getContentResolver().insert(Uri.parse("content://sms/sent"), values);

    }

    @Override
    public boolean checkIfDefaultSMS() {
        boolean isDefault = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String myPackageName = context.getPackageName();
            isDefault = Telephony.Sms.getDefaultSmsPackage(context).equals(myPackageName);
            if (!isDefault) {
                Intent intent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
                intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, myPackageName);
                context.startActivity(intent);
            }
        }
        return isDefault;
    }
}
