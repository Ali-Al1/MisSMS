package io.alwan.missms.helpers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.alwan.missms.entities.Contact;
import io.alwan.missms.entities.SMS;

/**
 * Created by Ali-Al1 on 08/07/2017.
 */

@Singleton
public class AppManagerImpl implements AppManager {
    private DataManager dataManager;
    private AndroidManager androidManager;

    @Inject
    AppManagerImpl(DataManager dataManager, AndroidManager androidManager) {
        this.dataManager = dataManager;
        this.androidManager = androidManager;
    }

    @Override
    public void sendMessage(String textToSend, String addressToSendTo) {
        androidManager.sendMessage(textToSend, addressToSendTo);
    }

    @Override
    public boolean checkIfDefaultSMS() {
        return androidManager.checkIfDefaultSMS();
    }

    @Override
    public boolean checkPermissionAndRequest(String permission) {
        return androidManager.checkPermissionAndRequest(permission);
    }

    @Override
    public Contact getContact(String number) {
        return androidManager.getContact(number);
    }

    @Override
    public Contact getProfile() {
        return androidManager.getProfile();
    }

    @Override
    public String getPersonUri(String contactId) {
        return androidManager.getPersonUri(contactId);
    }

    @Override
    public void updateNotifications(boolean silent) {
        dataManager.updateNotifications(silent);
    }

    @Override
    public ArrayList<SMS> getConversation(String address) {
        return dataManager.getConversation(address);
    }

    @Override
    public ArrayList<SMS> getConversations() {
        return dataManager.getConversations();
    }

    @Override
    public void updateNotifications(boolean silent, HashMap<String, ArrayList<SMS>> unreadMessages) {

    }

    @Override
    public void dismissThread(String threadId) {
        androidManager.dismissThread(threadId);
    }

    @Override
    public void copyText(String text) {
        androidManager.copyText(text);
    }

    @Override
    public ArrayList<SMS> getSmsList(String uriString, String[] columns, String selection, String[] selectionArgs, String sortOrder) {
        return dataManager.getSmsList(uriString, columns, selection, selectionArgs, sortOrder);
    }

    @Override
    public HashMap<String, ArrayList<SMS>> getSmsHash(String uriString, String[] columns, String selection, String[] selectionArgs, String sortOrder) {
        return dataManager.getSmsHash(uriString, columns, selection, selectionArgs, sortOrder);
    }

    @Override
    public void writeSMS(String senderNum, String message, String location) {
        dataManager.writeSMS(senderNum, message, location);
    }

    @Override
    public void markThreadAsRead(String threadId) {
        dataManager.markThreadAsRead(threadId);
    }

    @Override
    public void deleteThread(String threadId) {
        dataManager.deleteThread(threadId);
    }

    @Override
    public void deleteMessageById(String id) {
        dataManager.deleteMessageById(id);
    }

    @Override
    public void updateStatus(String address, String body, int error) {
        dataManager.updateStatus(address, body, error);
    }

    @Override
    public void unreadMessageById(String id) {
        dataManager.unreadMessageById(id);
    }
}
