package io.alwan.missms.injections.modules;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.alwan.missms.helpers.AndroidManager;
import io.alwan.missms.helpers.AndroidManagerImpl;
import io.alwan.missms.helpers.AppManager;
import io.alwan.missms.helpers.AppManagerImpl;
import io.alwan.missms.helpers.DataManager;
import io.alwan.missms.helpers.DataManagerImpl;
import io.alwan.missms.helpers.android.ContactHelper;
import io.alwan.missms.helpers.android.ContactHelperImpl;
import io.alwan.missms.helpers.android.NotificationHelper;
import io.alwan.missms.helpers.android.NotificationHelperImpl;
import io.alwan.missms.helpers.android.PermissionHelper;
import io.alwan.missms.helpers.android.PermissionHelperImpl;
import io.alwan.missms.helpers.android.SmsHelper;
import io.alwan.missms.helpers.android.SmsHelperImpl;
import io.alwan.missms.helpers.db.DbHelper;
import io.alwan.missms.helpers.db.DbHelperImpl;
import io.alwan.missms.injections.ApplicationContext;
import io.alwan.missms.views.MainActivity;

/**
 * Created by Ali-Al1 on 06/07/2017.
 */

@Module
public class AppModule {

    private final Application app;
    private MainActivity mainActivity;


    public AppModule(Application application) {
        app = application;
    }

    public void attachMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return app;
    }

    @Provides
    MainActivity provideActivity() {
        return mainActivity;
    }

    @Provides
    @Singleton
    AppManager provideAppManager(AppManagerImpl appManager) {
        return appManager;
    }

    @Provides
    @Singleton
    AndroidManager provideAndroidManager(AndroidManagerImpl androidManager) {
        return androidManager;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(DataManagerImpl dataManager) {
        return dataManager;
    }

    @Provides
    @Singleton
    ContactHelper provideContactHelper(ContactHelperImpl contactHelper) {
        return contactHelper;
    }

    @Provides
    @Singleton
    NotificationHelper provideNotificationHelper(NotificationHelperImpl notificationHelper) {
        return notificationHelper;
    }

    @Provides
    @Singleton
    PermissionHelper providePermissionHelper(PermissionHelperImpl permissionHelper) {
        return permissionHelper;
    }

    @Provides
    @Singleton
    SmsHelper provideSmsHelper(SmsHelperImpl smsHelper) {
        return smsHelper;
    }

    @Provides
    @Singleton
    DbHelper provideDbHelper(DbHelperImpl dbHelper) {
        return dbHelper;
    }


}
