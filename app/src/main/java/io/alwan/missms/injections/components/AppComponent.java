package io.alwan.missms.injections.components;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import io.alwan.missms.helpers.AppManager;
import io.alwan.missms.injections.ApplicationContext;
import io.alwan.missms.injections.modules.AppModule;
import io.alwan.missms.receivers_modules.NotificationReceivers;
import io.alwan.missms.receivers_modules.SmsReceiver;
import io.alwan.missms.views.MainActivity;
import io.alwan.missms.views.conversations.ConversationsFragment;

/**
 * Created by Ali-Al1 on 05/07/2017.
 */

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(MainActivity mainActivity);

    void inject(Application application);

    void inject(NotificationReceivers notificationReceivers);

    void inject(SmsReceiver smsReceiver);

    void inject(ConversationsFragment.EnterNumberDialogFragment enterNumberDialogFragment);

    @ApplicationContext
    Context context();

    AppManager appManager();

    MainActivity mainActivity();

}