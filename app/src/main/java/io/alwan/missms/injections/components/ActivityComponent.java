package io.alwan.missms.injections.components;

import dagger.Component;
import io.alwan.missms.injections.PerActivity;
import io.alwan.missms.injections.modules.ActivityModule;
import io.alwan.missms.views.conversation.ConversationFragment;
import io.alwan.missms.views.conversations.ConversationsFragment;

/**
 * Created by Ali-Al1 on 17/07/2017.
 */

//@Singleton
@PerActivity
@Component(dependencies = AppComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(ConversationFragment conversationFragment);

    void inject(ConversationsFragment conversationsFragment);

}
