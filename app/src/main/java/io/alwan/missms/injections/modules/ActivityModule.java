package io.alwan.missms.injections.modules;

import dagger.Module;
import dagger.Provides;
import io.alwan.missms.views.conversation.ConversationPresenter;
import io.alwan.missms.views.conversation.ConversationPresenterImpl;
import io.alwan.missms.views.conversations.ConversationsPresenter;
import io.alwan.missms.views.conversations.ConversationsPresenterImpl;

/**
 * Created by Ali-Al1 on 17/07/2017.
 */
@Module
public class ActivityModule {

    @Provides
    ConversationPresenter conversationPresenter(ConversationPresenterImpl conversationPresenter) {
        return conversationPresenter;
    }

    @Provides
    ConversationsPresenter conversationsPresenter(ConversationsPresenterImpl conversationsPresenter) {
        return conversationsPresenter;
    }
}
