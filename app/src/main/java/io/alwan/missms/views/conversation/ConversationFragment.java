package io.alwan.missms.views.conversation;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import javax.inject.Inject;

import io.alwan.missms.R;
import io.alwan.missms.helpers.AppManager;
import io.alwan.missms.views.base.BaseFragment;

public class ConversationFragment extends BaseFragment implements ConversationView {
    @Inject
    AppManager appManager;

    @Inject
    ConversationListAdapter adapter;

    @Inject
    ConversationPresenter conversationPresenter;

    private Bundle bundle;

    private ContentObserver contentObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            if (isAdded())
                update();
        }
    };

    public ConversationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivityComponent().inject(this);
        conversationPresenter.attach(this);

        bundle = this.getArguments();
        if (bundle != null) {
            conversationPresenter.setContact(bundle.getParcelable("contact"));
        }

        return inflater.inflate(R.layout.fragment_conversation, container, false);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (bundle != null) {
            conversationPresenter.initialise();
        }
    }

    public void registerContentObserver() {
        getContext().getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, contentObserver);
    }

    @Override
    public void onPause() {
        super.onPause();
        conversationPresenter.cleanUp();
    }

    public void unregisterContentObserver() {
        getContext().getContentResolver().unregisterContentObserver(contentObserver);
    }


    public void setActionBarTitle(String actionBarTitle) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(actionBarTitle);
    }


    public void initialiseViews() {
        FragmentActivity activity = getActivity();

        ImageButton sendButton = activity.findViewById(R.id.buttonSend);
        EditText smsBox = activity.findViewById(R.id.inputField);

        initialiseRecyclerView();

        sendButton.setOnClickListener((View v) -> {
            if (appManager.checkIfDefaultSMS()) {
                String smsMessage = smsBox.getText().toString();
                if (!smsMessage.equals(""))
                    conversationPresenter.sendSMS(smsMessage);
            }
        });
    }

    void update() {
        adapter.swap(conversationPresenter.getConversation());
    }

    private void initialiseRecyclerView() {
        FragmentActivity activity = getActivity();
        RecyclerView recyclerViewConversation = activity.findViewById(R.id.recycler_view_conversation);

        // use this if you want the RecyclerView to look like a vertical list view
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity.getApplicationContext());
        linearLayoutManager.setReverseLayout(true);
        recyclerViewConversation.setLayoutManager(linearLayoutManager);

        recyclerViewConversation.setAdapter(adapter);
        update();
    }
}
