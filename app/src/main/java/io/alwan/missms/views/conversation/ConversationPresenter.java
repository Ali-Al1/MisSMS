package io.alwan.missms.views.conversation;

import java.util.ArrayList;

import io.alwan.missms.entities.Contact;
import io.alwan.missms.entities.SMS;

/**
 * Created by Ali-Al1 on 16/07/2017.
 */

public interface ConversationPresenter {

    void initialise();

    void setContact(Contact contact);

    void attach(ConversationFragment conversationFragment);

    void cleanUp();

    void sendSMS(String smsMessage);

    ArrayList<SMS> getConversation();
}
