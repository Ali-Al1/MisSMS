package io.alwan.missms.views.conversations;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import javax.inject.Inject;

import io.alwan.missms.R;
import io.alwan.missms.entities.Contact;
import io.alwan.missms.entities.SMS;
import io.alwan.missms.helpers.AppManager;
import io.alwan.missms.injections.ApplicationContext;
import io.alwan.missms.views.MainActivity;

class ConversationsListAdapter extends RecyclerView.Adapter<ConversationsListAdapter.ViewHolder> {

    private ArrayList<SMS> smsList = new ArrayList<>();
    private Context context;
    private ActionMode actionMode;
    private ArrayList<Integer> selected = new ArrayList<>();
    private AppManager appManager;
    private MainActivity mainActivity;

    @Inject
    ConversationsListAdapter(@ApplicationContext Context context, AppManager appManager, MainActivity mainActivity) {
        this.context = context;
        this.appManager = appManager;
        this.mainActivity = mainActivity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.conversations_row, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //get sms and contact data
        SMS sms = smsList.get(position);
        Contact contact = appManager.getContact(sms.getAddress());
        String contactName = null;
        Bitmap bitmap = null;

        if (contact != null) {
            contactName = contact.getContactName();
            bitmap = contact.getContactDP();
        }

        //set address text
        if (contactName != null) holder.textViewAddress.setText(contactName);
        else holder.textViewAddress.setText(sms.getAddress());

        //set display picture
        if (selected.contains(position)) {
            holder.imageViewDP.setImageDrawable(
                    context.getResources().getDrawable(R.drawable.ic_tick_circle));
        } else {
            if (bitmap != null) holder.imageViewDP.setImageBitmap(bitmap);
            else holder.imageViewDP.setImageDrawable(
                    context.getResources().getDrawable(R.drawable.ic_account));
        }

        //set body text
        holder.textViewBody.setText(sms.getBody());

        //set status
        if (!sms.getRead().equals("1"))
            holder.imageViewStatus.setVisibility(View.GONE);
        else
            holder.imageViewStatus.setVisibility(View.VISIBLE);

        //set date text
        long milliSeconds = Long.parseLong(sms.getDate());
        String formattedDate = DateUtils.formatDateTime(context, milliSeconds, 0);
        holder.textViewDate.setText(formattedDate);

        //set listeners
        holder.conversationRow.setOnClickListener(v -> {
            if (actionMode != null) toggleSelection(position);
            else mainActivity.openConversation(contact);
        });

        holder.conversationRow.setOnLongClickListener(v -> {
            if (actionMode == null)
                actionMode = mainActivity.startSupportActionMode(new ActionModeCallback());

            toggleSelection(position);

            return true;
        });


    }


    @Override
    public int getItemCount() {
        return smsList.size();
    }

    void swap(ArrayList<SMS> conversationsList) {
        smsList.clear();
        smsList.addAll(conversationsList);
        notifyDataSetChanged();
    }

    private void readSelectedItems() {
        for (int position : selected) {
            appManager.markThreadAsRead(smsList.get(position).getThreadId());
        }
    }

    private void toggleSelection(int position) {
        if (selected.contains(position)) selected.remove((Integer) position);
        else selected.add(position);

        actionMode.invalidate();

        actionMode.setTitle(selected.size() + " conversations selected");

        if (selected.size() == 0)
            actionMode.finish();

        notifyDataSetChanged();
    }

    private void deleteSelectedItems() {
        Collections.sort(selected);
        for (int i = selected.size() - 1; i >= 0; i--) {
            appManager.deleteThread(smsList.get(selected.get(i)).getThreadId());
            smsList.remove((int) selected.remove(i));
        }
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        // 1. Declare your Views here

        LinearLayout conversationRow;
        ImageView imageViewDP;
        TextView textViewAddress;
        TextView textViewDate;
        TextView textViewBody;
        ImageView imageViewStatus;

        ViewHolder(View itemView) {
            super(itemView);
            // 2. Define your Views here
            conversationRow = itemView.findViewById(R.id.conversationRow);
            imageViewDP = itemView.findViewById(R.id.imageViewDP);
            textViewAddress = itemView.findViewById(R.id.textViewAddress);
            textViewDate = itemView.findViewById(R.id.textViewTime);
            textViewBody = itemView.findViewById(R.id.textViewBody);
            imageViewStatus = itemView.findViewById(R.id.imageViewStatus);
        }
    }

    private class ActionModeCallback implements ActionMode.Callback {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            mode.setTitleOptionalHint(false);
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.conversations_context_menu, menu);
            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
//
////            boolean sent = false;
//
//            boolean unread = false;
//
//            boolean read = false;
//
//            for (int smsposition : selected) {
////                if (smsList.get(smsposition).getType().equals("sent"))
////                    sent = true;
////                else {
//                    if (smsList.get(smsposition).getRead().equals("0"))
//                        unread = true;
//                    else
//                        read = true;
//
////                }
//
//            }
//
//            MenuItem readButton = menu.findItem(R.id.read_conversation);
//
//            if (read && readButton.isVisible()){
//                readButton.setVisible(false);
//                return true;
//            }
//
//            if (!read && !readButton.isVisible()) {
//                readButton.setVisible(true);
//                return true;
//            }
//
            return false; // Return false if nothing is done
        }


        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.delete_conversation:
                    deleteSelectedItems();
                    mode.finish();
                    return true;
                case R.id.read_conversation:
                    readSelectedItems();
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            actionMode = null;
            selected.clear();
            notifyDataSetChanged();
        }
    }
}