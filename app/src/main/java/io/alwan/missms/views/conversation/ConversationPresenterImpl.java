package io.alwan.missms.views.conversation;

import java.util.ArrayList;

import javax.inject.Inject;

import io.alwan.missms.entities.Contact;
import io.alwan.missms.entities.SMS;
import io.alwan.missms.helpers.AppManager;

/**
 * Created by Ali-Al1 on 16/07/2017.
 */

public class ConversationPresenterImpl implements ConversationPresenter {

    private Contact contact;

    private ArrayList<SMS> conversationList;

    private AppManager appManager;
    private ConversationView conversationView;

    @Inject
    ConversationPresenterImpl(AppManager appManager) {
        this.appManager = appManager;
    }

    @Override
    public void initialise() {
        conversationList = getConversation();

        markThreadAsRead();

        setActionBarTitle();

        conversationView.initialiseViews();

        conversationView.registerContentObserver();
    }

    private void markThreadAsRead() {
        if (conversationList.size() != 0) {
            SMS sms = conversationList.get(0);
            if (sms != null) {
                appManager.markThreadAsRead(sms.getThreadId());
            }
        }
    }

    private void setActionBarTitle() {
        String actionBarTitle;
        String contactName = contact.getContactName();
        String contactAddress = contact.getContactAddress();

        if (contactName != null) actionBarTitle = contactName;
        else actionBarTitle = contactAddress;

        conversationView.setActionBarTitle(actionBarTitle);
    }

    @Override
    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @Override
    public void attach(ConversationFragment conversationFragment) {
        this.conversationView = conversationFragment;
    }

    @Override
    public void cleanUp() {
        conversationView.unregisterContentObserver();
    }

    @Override
    public void sendSMS(String smsMessage) {
        appManager.sendMessage(smsMessage, contact.getContactAddress());
    }

    @Override
    public ArrayList<SMS> getConversation() {
        return appManager.getConversation(contact.getContactAddress());
    }
}
