package io.alwan.missms.views.conversations;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import java.util.ArrayList;

import javax.inject.Inject;

import io.alwan.missms.MisSMS;
import io.alwan.missms.R;
import io.alwan.missms.entities.SMS;
import io.alwan.missms.helpers.AppManager;
import io.alwan.missms.views.MainActivity;
import io.alwan.missms.views.base.BaseFragment;

public class ConversationsFragment extends BaseFragment implements ConversationsView {

    @Inject
    ConversationsPresenter conversationsPresenter;

    @Inject
    ConversationsListAdapter adapter;

    ContentObserver contentObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            if (isAdded())
                update();
        }
    };

    public ConversationsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivityComponent().inject(this);
        conversationsPresenter.attach(this);

        return inflater.inflate(R.layout.fragment_conversations, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        conversationsPresenter.initialise();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.app_name);

        getContext().getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, contentObserver);
    }

    @Override
    public void onPause() {
        super.onPause();
        getContext().getContentResolver().unregisterContentObserver(contentObserver);
    }

    public void initialiseRecyclerView() {
        RecyclerView recyclerViewConversations =
                getActivity().findViewById(R.id.recycler_view_conversations);

        // use this if you want the RecyclerView to look like a vertical list view
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewConversations.setLayoutManager(linearLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                recyclerViewConversations.getContext(), linearLayoutManager.getOrientation());
        recyclerViewConversations.addItemDecoration(dividerItemDecoration);

        adapter.swap(conversationsPresenter.getConversations());
        recyclerViewConversations.setAdapter(adapter);
    }

    public void update() {
        new Handler().postDelayed(() -> {
            ArrayList<SMS> conversationsList =
                    conversationsPresenter.getConversations();
            adapter.swap(conversationsList);
        }, 1000);
    }

    public void initialiseFamView() {
        MainActivity mainActivity = (MainActivity) getActivity();
        FloatingActionsMenu floatingActionsMenu =
                mainActivity.findViewById(R.id.fam);
        FloatingActionButton pickContact =
                mainActivity.findViewById(R.id.pick_contact);
        FloatingActionButton enterNumber =
                mainActivity.findViewById(R.id.enter_number);

        pickContact.setOnClickListener((View) -> {
            mainActivity.pickContact();
            floatingActionsMenu.collapse();
        });
        enterNumber.setOnClickListener((View) -> {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            Fragment prev = getFragmentManager().findFragmentByTag("dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);

            EnterNumberDialogFragment enterNumberDialog = new EnterNumberDialogFragment();
            enterNumberDialog.show(ft, "dialog");
        });
        floatingActionsMenu.collapse();
    }

    public static class EnterNumberDialogFragment extends DialogFragment {
        @Inject
        AppManager appManager;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            ((MisSMS) getActivity().getApplication()).getAppComponent()
                    .inject(this);

            View v = inflater.inflate(R.layout.fragment_dialog, container, false);
            getDialog().setTitle("Enter Number");

            EditText phoneNumber = v.findViewById(R.id.editTextPhoneNumber);
            Button buttonConfirm = v.findViewById(R.id.buttonConfirm);
            buttonConfirm.setOnClickListener(v1 -> {
                String number = phoneNumber.getText().toString();
                if (!number.equals("")) {
                    dismiss();
                    ((MainActivity) getActivity()).openConversation(appManager.getContact(number));
                }
            });

            Button buttonCancel = v.findViewById(R.id.buttonCancel);
            buttonCancel.setOnClickListener(v1 -> dismiss());

            return v;
        }

    }
}