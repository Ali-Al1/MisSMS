package io.alwan.missms.views.conversation;

/**
 * Created by Ali-Al1 on 17/07/2017.
 */

public interface ConversationView {

    void initialiseViews();

    void registerContentObserver();

    void setActionBarTitle(String actionBarTitle);

    void unregisterContentObserver();
}
