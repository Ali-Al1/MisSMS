package io.alwan.missms.views.base;

import android.os.Bundle;
import android.support.annotation.Nullable;

import io.alwan.missms.MisSMS;
import io.alwan.missms.injections.components.ActivityComponent;
import io.alwan.missms.injections.components.DaggerActivityComponent;
import io.alwan.missms.injections.modules.ActivityModule;

/**
 * Created by Ali-Al1 on 17/07/2017.
 */

public class BaseFragment extends android.support.v4.app.Fragment {

    private ActivityComponent activityComponent;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule())
                .appComponent(((MisSMS) getActivity().getApplication()).getAppComponent())
                .build();
    }

    public ActivityComponent getActivityComponent() {
        return activityComponent;
    }
}
