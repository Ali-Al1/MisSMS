package io.alwan.missms.views.conversations;

import java.util.ArrayList;

import javax.inject.Inject;

import io.alwan.missms.entities.SMS;
import io.alwan.missms.helpers.AppManager;

/**
 * Created by Ali-Al1 on 17/07/2017.
 */

public class ConversationsPresenterImpl implements ConversationsPresenter {

    private ConversationsView conversationsView;
    private AppManager appManager;

    @Inject
    ConversationsPresenterImpl(AppManager appManager) {
        this.appManager = appManager;
    }

    @Override
    public void attach(ConversationsView conversationsView) {
        this.conversationsView = conversationsView;
    }

    @Override
    public ArrayList<SMS> getConversations() {
        return appManager.getConversations();

    }

    @Override
    public void initialise() {
        conversationsView.initialiseRecyclerView();
        conversationsView.initialiseFamView();
    }
}
