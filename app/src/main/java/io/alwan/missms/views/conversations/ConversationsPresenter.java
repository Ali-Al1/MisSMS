package io.alwan.missms.views.conversations;

import java.util.ArrayList;

import io.alwan.missms.entities.SMS;

/**
 * Created by Ali-Al1 on 17/07/2017.
 */

public interface ConversationsPresenter {


    void attach(ConversationsView conversationsView);

    ArrayList<SMS> getConversations();

    void initialise();
}
