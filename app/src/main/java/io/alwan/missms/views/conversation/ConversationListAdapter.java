package io.alwan.missms.views.conversation;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;

import javax.inject.Inject;

import io.alwan.missms.R;
import io.alwan.missms.entities.SMS;
import io.alwan.missms.helpers.AppManager;
import io.alwan.missms.injections.ApplicationContext;
import io.alwan.missms.views.MainActivity;

class ConversationListAdapter extends RecyclerView.Adapter<ConversationListAdapter.ViewHolder> {

    private MainActivity mainActivity;
    private AppManager appManager;

    private ArrayList<SMS> smsList = new ArrayList<>();
    private Context context;
    private ActionMode actionMode;
    private ArrayList<Integer> selected = new ArrayList<>();

    @Inject
    ConversationListAdapter(@ApplicationContext Context context, MainActivity mainActivity,
                            AppManager appManager) {
        this.context = context;
        this.mainActivity = mainActivity;
        this.appManager = appManager;
    }


    @Override
    public int getItemViewType(int position) {
        if (smsList.get(position).getType().equals("sent")) return 0;
        else return 1;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (viewType == 0)
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.conversation_sent, parent, false);
        else
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.conversation_received, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final SMS sms = smsList.get(position);
        holder.textViewBody.setText(sms.getBody());

        long milliSeconds = Long.parseLong(sms.getDate());
        String formattedDate = DateUtils.formatDateTime(context, milliSeconds, DateUtils.FORMAT_SHOW_TIME);


        String errorCode = sms.getErrorCode();

        switch (errorCode) {
            case "0":
                break;
            case "-1":
                formattedDate = formattedDate + " " + context.getString(R.string.tick);
                break;
            default:
                formattedDate = formattedDate + " " + context.getString(R.string.cross);
        }
        holder.textViewTime.setText(formattedDate);


        DateFormat fmt = SimpleDateFormat.getDateInstance();

        if (position != smsList.size() - 1) {
            long previousMessage = Long.parseLong(smsList.get(position + 1).getDate());
            Boolean sameDate = fmt.format(milliSeconds).equals(fmt.format(previousMessage));
            if (!sameDate) {
                holder.textViewDate.setText(fmt.format(milliSeconds));
                holder.dateSeparator.setVisibility(View.VISIBLE);
            } else
                holder.dateSeparator.setVisibility(View.GONE);

        } else {
            holder.textViewDate.setText(fmt.format(milliSeconds));
            holder.dateSeparator.setVisibility(View.VISIBLE);
        }


        if (selected.contains(position)) {
            ViewCompat.setBackgroundTintList(holder.messageContainer,
                    ContextCompat.getColorStateList(context, R.color.blue));
        } else {
            if (sms.getType().equals("sent"))
                ViewCompat.setBackgroundTintList(holder.messageContainer,
                        ContextCompat.getColorStateList(context, R.color.dark_grey));
            else
                ViewCompat.setBackgroundTintList(holder.messageContainer,
                        ContextCompat.getColorStateList(context, R.color.white));
        }

        holder.messageContainer.setOnClickListener(v -> {
            if (actionMode != null) toggleSelection(position);
        });

        holder.messageContainer.setOnLongClickListener(v -> {
            if (actionMode == null)
                actionMode = mainActivity.startSupportActionMode(new ActionModeCallback());

            toggleSelection(position);

            return true;
        });

    }

    @Override
    public int getItemCount() {
        return smsList.size();
    }

    void swap(ArrayList<SMS> newList) {
        smsList.clear();
        smsList.addAll(newList);
        notifyDataSetChanged();
    }

    private void copyItem() {
        if (selected.size() == 1)
            appManager.copyText(smsList.get(selected.get(0)).getBody());
    }

    private void unreadSelectedItems() {
        for (int position : selected) {
            appManager.unreadMessageById(smsList.get(position).getId());
        }
        appManager.updateNotifications(true);
    }

    private void toggleSelection(int position) {
        if (selected.contains(position)) selected.remove((Integer) position);
        else selected.add(position);

        actionMode.invalidate();
        actionMode.setTitle(selected.size() + " messages selected");


        if (selected.size() == 0) {
            actionMode.finish();
        }

        notifyDataSetChanged();
    }

    private void deleteSelectedItems() {
        Collections.sort(selected);

        for (int i = selected.size() - 1; i >= 0; i--) {
            appManager.deleteMessageById(smsList.get(selected.get(i)).getId());
            smsList.remove((int) selected.remove(i));
        }

        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        // 1. Declare your Views here
        LinearLayout conversationRow;
        RelativeLayout dateSeparator;
        TextView textViewDate;
        TextView textViewBody;
        TextView textViewTime;
        LinearLayout messageContainer;

        ViewHolder(View itemView) {
            super(itemView);
            // 2. Define your Views here
            conversationRow = itemView.findViewById(R.id.conversationRow);
            dateSeparator = itemView.findViewById(R.id.dateSeparator);
            textViewDate = itemView.findViewById(R.id.textViewDate);
            textViewBody = itemView.findViewById(R.id.textViewBody);
            textViewTime = itemView.findViewById(R.id.textViewTime);
            messageContainer = itemView.findViewById(R.id.messageContainer);
        }
    }

    private class ActionModeCallback implements ActionMode.Callback {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            mode.setTitleOptionalHint(true);
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.conversation_context_menu, menu);
            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            boolean sent = false;

            for (int position : selected) {
                if (smsList.get(position).getType().equals("sent"))
                    sent = true;
            }

            MenuItem unreadMessage = menu.findItem(R.id.unread_message);
            MenuItem copyMessage = menu.findItem(R.id.copy_message);

            if (selected.size() == 1 && !copyMessage.isVisible())
                copyMessage.setVisible(true);

            if (selected.size() != 1 && copyMessage.isVisible())
                copyMessage.setVisible(false);

            if (sent && unreadMessage.isVisible())
                unreadMessage.setVisible(false);

            if (!sent && !unreadMessage.isVisible())
                unreadMessage.setVisible(true);

            return true;
        }


        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.delete_message:
                    deleteSelectedItems();
                    mode.finish();
                    return true;
                case R.id.unread_message:
                    unreadSelectedItems();
                    mode.finish();
                    return true;
                case R.id.copy_message:
                    copyItem();
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            actionMode = null;
            selected.clear();
            notifyDataSetChanged();
        }
    }
}
