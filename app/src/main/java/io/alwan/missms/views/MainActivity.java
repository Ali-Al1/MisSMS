package io.alwan.missms.views;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.getbase.floatingactionbutton.FloatingActionButton;

import javax.inject.Inject;

import io.alwan.missms.MisSMS;
import io.alwan.missms.R;
import io.alwan.missms.entities.Contact;
import io.alwan.missms.helpers.AppManager;
import io.alwan.missms.views.conversation.ConversationFragment;
import io.alwan.missms.views.conversations.ConversationsFragment;

//import io.alwan.missms.injections.components.DaggerAppComponent;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final int RESULT_PICK_CONTACT = 22;
    public Fragment mainFragment;
    FragmentManager fm;
    Boolean smsPermission;
    Contact currentContact;

    @Inject
    AppManager appManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        setContentView(R.layout.activity_main);

        final MisSMS application = (MisSMS) getApplication();
        application.getAppComponent().inject(this);
        application.getAppModule().attachMainActivity(this);

//        androidManager.copyText("test4");

        initDrawerAndToolbar();

        //Open conversation if extras are passed in
        openConversation();

//        HelperSMS.initialiseSMSSender();

        //Initialise my views
        initialiseViews(savedInstanceState);

        appManager.checkIfDefaultSMS();
    }

    private void initialiseViews(Bundle savedInstanceState) {
        fm = getSupportFragmentManager();
        initialiseFloatingActionMenu();
        initialiseProfile();

        //Restore instance or create new one
        if (savedInstanceState != null) restoreView(savedInstanceState);
        else initialiseActivity();
    }

    private void openConversation() {
        currentContact = null;
        Bundle extras = getIntent().getExtras();
        String value;
        if (extras != null) {
            value = extras.getString("address");
            if (value != null)
                currentContact = appManager.getContact(value);
        }
    }

    private void initDrawerAndToolbar() {
        //Generated view initialisation code
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void initialiseProfile() {
        Contact myContact = appManager.getProfile();

        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);

        if (myContact != null) {
            String contactName = myContact.getContactName();
            if (contactName != null) {
                TextView textViewUserName = headerView.findViewById(R.id.textViewUserName);
                textViewUserName.setText(contactName);
            }
            String contactNumber = myContact.getContactAddress();
            if (contactNumber != null) {
                TextView textViewUserNumber = headerView.findViewById(R.id.textViewUserNumber);
                textViewUserNumber.setText(contactNumber);
            }
            Bitmap contactDP = myContact.getContactDP();
            if (contactDP != null) {
                ImageView imageViewUserDP = headerView.findViewById(R.id.imageViewUserDP);
                imageViewUserDP.setImageBitmap(contactDP);
            }
        }
    }

    private void initialiseFloatingActionMenu() {
        FloatingActionButton enterNumber = findViewById(R.id.enter_number);
        if (enterNumber != null) {
            FloatingActionButton pickContact = findViewById(R.id.pick_contact);
            pickContact.setOnClickListener(view -> pickContact());
        }
    }

    private void initialiseConversationsFragment() {
        if (permissionCheckSMS())
            switchMainFragment(new ConversationsFragment(), false);
    }

    private void restoreView(Bundle savedInstanceState) {
        mainFragment = fm.getFragment(savedInstanceState, "mainFragment");
        switchMainFragment(mainFragment, false);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
//        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        //Save the fragment's instance
        if (mainFragment != null) {
            if (mainFragment.isAdded()) {
                getSupportFragmentManager().putFragment(outState, "mainFragment", mainFragment);
            }
        }
        super.onSaveInstanceState(outState);
    }

    private void initialiseActivity() {
        smsPermission = permissionCheckSMS();
        boolean contactsPermission = permissionCheckContacts();

        if (smsPermission && contactsPermission)
            initialiseConversationsFragment();

        else if (smsPermission && !contactsPermission) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CONTACTS}, 1);
            initialiseConversationsFragment();
        } else if (!smsPermission && !contactsPermission)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS,
                    Manifest.permission.READ_CONTACTS}, 1);

        else if (!smsPermission && contactsPermission)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS}, 1);

    }

    private boolean permissionCheckContacts() {
        return ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED;
    }

    public boolean permissionCheckSMS() {
        return ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        boolean granted = false;

        for (int grantedResult : grantResults) {
            if (grantedResult == PackageManager.PERMISSION_GRANTED) {
                granted = true;
            }
        }
        if (granted)
            initialiseConversationsFragment();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_conversations) {
            initialiseConversationsFragment();
        }
//        else if (id == R.id.nav_settings) {
//
//        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void switchMainFragment(Fragment fragment, Boolean back) {
        if (back)
            fm.beginTransaction().replace(R.id.main_fragment_container, fragment)
                    .addToBackStack(null).commitAllowingStateLoss();
        else
            fm.beginTransaction().replace(R.id.main_fragment_container, fragment).commitAllowingStateLoss();
        mainFragment = fragment;
    }

    public void openConversation(Contact contact) {
        Bundle arguments = new Bundle();
        arguments.putParcelable("contact", contact);

        ConversationFragment conversationFragment = new ConversationFragment();
        conversationFragment.setArguments(arguments);

        switchMainFragment(conversationFragment, true);
    }

    public void pickContact() {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, RESULT_PICK_CONTACT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check whether the result is ok
        if (resultCode == RESULT_OK) {
            // Check for the request code, we might be using multiple startActivityForResult
            switch (requestCode) {
                case RESULT_PICK_CONTACT:
                    contactPicked(data);
                    break;
            }
        } else {
            Log.e("MainActivity", "Failed to pick contact");
        }
    }

    private void contactPicked(Intent data) {
        Cursor cursor;
        try {
            String phoneNo;
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            //Query the content uri
            cursor = getContentResolver().query(uri, null, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();
            // column index of the phone number
            int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            phoneNo = cursor.getString(phoneIndex);
            cursor.close();
            // start conversation
            currentContact = appManager.getContact(phoneNo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (currentContact != null) {
            openConversation(currentContact);
            currentContact = null;
        }
    }
}