package io.alwan.missms.entities;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class Contact implements Parcelable {

    private Bitmap contactDP;
    private String contactName;
    private String contactAddress;
    private String contactId;

    public Contact() {}

    public Bitmap getContactDP() {
        return contactDP;
    }

    public void setContactDP(Bitmap contactDP) {
        this.contactDP = contactDP;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(contactDP);
        dest.writeString(contactName);
        dest.writeString(contactAddress);
        dest.writeString(contactId);
    }    private Contact(Parcel in) {
        contactDP = (Bitmap) in.readValue(Bitmap.class.getClassLoader());
        contactName = in.readString();
        contactAddress = in.readString();
        contactId = in.readString();
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Contact> CREATOR = new Parcelable.Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };
}