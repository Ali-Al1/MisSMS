package io.alwan.missms.receivers_modules;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.RemoteInput;
import android.telephony.SmsManager;
import android.widget.Toast;

import javax.inject.Inject;

import io.alwan.missms.MisSMS;
import io.alwan.missms.helpers.AppManager;

import static io.alwan.missms.helpers.android.NotificationHelperImpl.KEY_TEXT_REPLY;
import static io.alwan.missms.helpers.android.NotificationHelperImpl.READ;
import static io.alwan.missms.helpers.android.NotificationHelperImpl.SEND;
import static io.alwan.missms.helpers.android.NotificationHelperImpl.SENT;


public class NotificationReceivers extends BroadcastReceiver {

    @Inject
    AppManager appManager;

    public NotificationReceivers() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();

        if (extras != null) {

            String value;

            ((MisSMS) context.getApplicationContext()).getAppComponent().inject(this);

            switch (intent.getAction()) {
                case READ:
                    value = extras.getString("threadForRead");

                    appManager.markThreadAsRead(value);
                    break;

                case SEND:
                    Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);

                    value = extras.getString("addressForSend");

                    String inputString =
                            remoteInput.getCharSequence(KEY_TEXT_REPLY).toString();

                    appManager.sendMessage(inputString, value);

                    String threadID = extras.getString("threadForSend");

                    appManager.markThreadAsRead(threadID);
                    break;

                case SENT:
                    int resultCode = getResultCode();

                    appManager.updateStatus(intent.getStringExtra("address"),
                            intent.getStringExtra("body"), resultCode);

                    switch (resultCode) {
                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                            Toast.makeText(context, "Failed to send",
                                    Toast.LENGTH_SHORT).show();
                            break;
                        case SmsManager.RESULT_ERROR_NO_SERVICE:
                            Toast.makeText(context, "No service",
                                    Toast.LENGTH_SHORT).show();
                            break;
                        case SmsManager.RESULT_ERROR_NULL_PDU:
                            Toast.makeText(context, "Null PDU", Toast.LENGTH_SHORT).show();
                            break;
                        case SmsManager.RESULT_ERROR_RADIO_OFF:
                            Toast.makeText(context, "Radio off", Toast.LENGTH_SHORT).show();
                            break;
                    }
            }
        }
    }
}