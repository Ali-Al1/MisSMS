package io.alwan.missms.receivers_modules;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import javax.inject.Inject;

import io.alwan.missms.MisSMS;
import io.alwan.missms.helpers.AppManager;

import static android.content.Context.ACTIVITY_SERVICE;

public class SmsReceiver extends BroadcastReceiver {

    @Inject
    AppManager appManager;

    public void onReceive(Context context, Intent intent) {

        final Bundle bundle = intent.getExtras();
        if (bundle != null) {

            ((MisSMS) context.getApplicationContext()).getAppComponent().inject(this);

            final Object[] pdusObj = (Object[]) bundle.get("pdus");

            assert pdusObj != null;
            for (Object aPdusObj : pdusObj) {

                SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) aPdusObj);
                String senderNum = currentMessage.getDisplayOriginatingAddress();
                String message = currentMessage.getDisplayMessageBody();

                appManager.writeSMS(senderNum, message, "inbox");

                if (!isAppOpen(context)) {
                    appManager.updateNotifications(false);
                }

            } //end for loop
        }//null bundle
    }//end method

    private boolean isAppOpen(Context context) {
        //get current package name
        final String packageName = context.getPackageName();

        //get current foreground task's package name
        ActivityManager activityManager = (ActivityManager)
                context.getSystemService(ACTIVITY_SERVICE);
        ActivityManager.RunningTaskInfo foregroundTaskInfo =
                activityManager.getRunningTasks(1).get(0);
        final String foregroundTaskPackageName = foregroundTaskInfo.topActivity.getPackageName();

        //check if current app is open
        return packageName.equals(foregroundTaskPackageName);
    }

}//end class