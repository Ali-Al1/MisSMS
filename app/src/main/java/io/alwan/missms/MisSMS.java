package io.alwan.missms;

import android.app.Application;
import android.util.Log;

import io.alwan.missms.injections.components.AppComponent;
import io.alwan.missms.injections.components.DaggerAppComponent;
import io.alwan.missms.injections.modules.AppModule;

import static android.content.ContentValues.TAG;

/**
 * Created by Ali-Al1 on 06/07/2017.
 */

public class MisSMS extends Application {

    private AppComponent appComponent;


    private AppModule appModule;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d(TAG, "onCreate: is this happening? test");
        appModule = new AppModule(this);
        appComponent = DaggerAppComponent.builder()
                .appModule(appModule).build();

        appComponent.inject(this);

    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public AppModule getAppModule() {
        return appModule;
    }
}
